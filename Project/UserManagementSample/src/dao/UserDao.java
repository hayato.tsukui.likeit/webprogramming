package dao;

import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

import model.User;

/**
 * ユーザテーブル用のDao87urt5m,ｖｆ
 *
 * @author takano
 */
public class UserDao {

	/**
	 * ログインIDとパスワードに紐づくユーザ情報を返す S
	 * 
	 * @param loginId
	 * @param password
	 * @return
	 */
	public User findByLoginInfo(String loginId, String password) {
		Connection conn = null;
		try {
			// データベースへ接続
			conn = DBManager.getConnection();

			// SELECT文を準備
			String sql = "SELECT * FROM user WHERE login_id = ? and password = ?";

			// SELECTを実行し、結果表を取得
			PreparedStatement pStmt = conn.prepareStatement(sql);
			pStmt.setString(1, loginId);
			pStmt.setString(2, password);

			ResultSet rs = pStmt.executeQuery();

			// 主キーに紐づくレコードは1件のみなので、rs.next()は1回だけ行う
			if (!rs.next()) {
				return null;
			}

			// 必要なデータのみインスタンスのフィールドに追加
			String loginIdData = rs.getString("login_id");
			String nameData = rs.getString("name");
			return new User(loginIdData, nameData);

		} catch (SQLException e) {
			e.printStackTrace();
			return null;
		} finally {
			// データベース切断
			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
					e.printStackTrace();
					return null;
				}
			}
		}
	}

	/**
	 * 全てのユーザ情報を取得する
	 *
	 * @return
	 */
	public List<User> findAll() {
		Connection conn = null;
		List<User> userList = new ArrayList<User>();

		try {
			// データベースへ接続
			conn = DBManager.getConnection();

			// SELECT文を準備
			// TODO: 未実装：管理者以外を取得するようSQLを変更する
			String sql = "SELECT * FROM user WHERE is_admin = false";

			// SELECTを実行し、結果表を取得
			Statement stmt = conn.createStatement();
			ResultSet rs = stmt.executeQuery(sql);

			// 結果表に格納されたレコードの内容を
			// Userインスタンスに設定し、ArrayListインスタンスに追加
			while (rs.next()) {
				int id = rs.getInt("id");
				String loginId = rs.getString("login_id");
				String name = rs.getString("name");
				Date birthDate = rs.getDate("birth_date");
				String password = rs.getString("password");
				boolean isAdmin = rs.getBoolean("is_admin");
				Timestamp createDate = rs.getTimestamp("create_date");
				Timestamp updateDate = rs.getTimestamp("update_date");
				User user = new User(id, loginId, name, birthDate, password, isAdmin, createDate, updateDate);

				userList.add(user);
			}
		} catch (SQLException e) {
			e.printStackTrace();
			return null;
		} finally {
			// データベース切断
			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
					e.printStackTrace();
					return null;
				}
			}
		}
		return userList;
	}

	public void add(String loginId, String userName, String birthDay, String password) {
		Connection conn = null;
		try {
			// データベースへ接続
			conn = DBManager.getConnection();

			// SELECT文を準備
			String sql = "insert into user (login_id,name,birth_date,password,is_admin,create_date,update_date) values (?,?,?,?,default,now(),now())";

			// SELECTを実行し、結果表を取得
			PreparedStatement pStmt = conn.prepareStatement(sql);

			pStmt.setString(1, loginId);
			pStmt.setString(2, userName);
			pStmt.setString(3, birthDay);
			pStmt.setString(4, password);
			pStmt.executeUpdate();

		} catch (SQLException e) {
			e.printStackTrace();
			return;

		} finally {
			// データベース切断
			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
		}
	}

	// TODO 自動生成されたメソッド・スタブ

	public static User findById(String ID) {

		Connection conn = null;
		User user = new User();

		try {
			// データベースへ接続
			conn = DBManager.getConnection();

			// SELECT文を準備
			// TODO: 未実装：管理者以外を取得するようSQLを変更する
			String sql = "select * from user where id = ? ";

			// SELECTを実行し、結果表を取得

			PreparedStatement pStmt = conn.prepareStatement(sql);
			pStmt.setString(1, ID);
			ResultSet rs = pStmt.executeQuery();

			if (!rs.next()) {
				return null;

			}
			// 必要なデータのみインスタンスのフィールドに追加
			int id = rs.getInt("id");
			String loginId = rs.getString("login_id");
			String name = rs.getString("name");
			Date birthDate = rs.getDate("birth_date");
			String password = rs.getString("password");
			boolean isAdmin = rs.getBoolean("is_admin");
			Timestamp createDate = rs.getTimestamp("create_date");
			Timestamp updateDate = rs.getTimestamp("update_date");
			
			
			
			user = new User(id, loginId, name, birthDate, password, isAdmin, createDate, updateDate);

		} catch (SQLException e) {
			e.printStackTrace();
			return null;
		} finally {
			// データベース切断
			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
					e.printStackTrace();
					return null;
				}
			}
		}
		return user;

	}

	public void update(String password, String name, String birthDate, String id) {
		Connection conn = null;
		try {
			// データベースへ接続
			conn = DBManager.getConnection();

			// SELECT文を準備
			String sql = "update user set password = ?, name = ? , birth_date = ?  where id = ? ";

			// SELECTを実行し、結果表を取得
			PreparedStatement pStmt = conn.prepareStatement(sql);

			pStmt.setString(1, password);
			pStmt.setString(2, name);
			pStmt.setString(3, birthDate);
			pStmt.setString(4, id);
			pStmt.executeUpdate();

		} catch (SQLException e) {
			e.printStackTrace();
			return;

		} finally {
			// データベース切断
			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
		}
	}

	public static void delete(String ID) {
		Connection conn = null;
		try {
			// データベースへ接続
			conn = DBManager.getConnection();

			// SELECT文を準備
			String sql = "delete from user where id = ? ;";

			// SELECTを実行し、結果表を取得
			PreparedStatement pStmt = conn.prepareStatement(sql);

			pStmt.setString(1, ID);
			pStmt.executeUpdate();

		} catch (SQLException e) {
			e.printStackTrace();
			return;

		} finally {
			// データベース切断
			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
		}
	}

	public List<User> search(String loginID, String userName, String startDate, String endDate) {
		Connection conn = null;
		List<User> userList = new ArrayList<User>();

		try {
			// データベースへ接続
			conn = DBManager.getConnection();

			// SELECT文を準備
			// TODO: 未実装：管理者以外を取得するようSQLを変更する

			String sql = "select * from user where is_admin = false ";

			// SELECTを実行し、結果表を取得

			StringBuilder sb = new StringBuilder(sql);
			
			ArrayList<String> list = new ArrayList<String>();

			if (!loginID.equals("")) {
				sb.append(" and login_id = ? ");
				list.add(loginID);
			}
			// 49と同じ意味

			if (!userName.equals("")) {
				sb.append(" and name like ? ");
				list.add("%" + userName + "%");
				//pStmt.setString("%" + userName + "%");
			}

			if (!startDate.equals("")) {
				sb.append(" and birth_date >= ? ");
				list.add(startDate);
			}

			if (!endDate.equals("")) {
				sb.append(" and birth_date <= ? ");
				list.add(endDate);
			}
				

			PreparedStatement pStmt = conn.prepareStatement(sb.toString());
			
			int size = list.size();
			
			for(int i=0; i < size ;i++) {
			pStmt.setString(i+1, list.get(i));
			}
			

			ResultSet rs = pStmt.executeQuery();
			

			while (rs.next()) {
				int id = rs.getInt("id");
				String loginId = rs.getString("login_id");
				String name = rs.getString("name");
				Date birthDate = rs.getDate("birth_date");
				String password = rs.getString("password");
				boolean isAdmin = rs.getBoolean("is_admin");
				Timestamp createDate = rs.getTimestamp("create_date");
				Timestamp updateDate = rs.getTimestamp("update_date");
				User user = new User(id, loginId, name, birthDate, password, isAdmin, createDate, updateDate);

				userList.add(user);

			}
		} catch (SQLException e) {
			e.printStackTrace();
			return null;
		} finally {
			// データベース切断
			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
					e.printStackTrace();
					return null;
				}
			}
		}
		return userList;
		

	}

}
