package controller;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import dao.UserDao;
import model.User;
import util.PasswordEncorder;

/**
 * Servlet implementation class UserUpdateServlet
 */
@WebServlet("/UserUpdateServlet")
public class UserUpdateServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public UserUpdateServlet() {
		super();
		// TODO Auto-generated constructor stub
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		// TODO Auto-generated method stub
		request.setCharacterEncoding("UTF-8");
		// URLからGETパラメータとしてIDを受け取る

		HttpSession session = request.getSession();

		User loginSession = (User) session.getAttribute("userInfo");

		if (loginSession == null) {
			response.sendRedirect("LoginServlet");

		} else {

			String id = request.getParameter("id");

			// 確認用：idをコンソールに出力
			System.out.println(id);

			// TODO 未実装：idを引数にして、idに紐づくユーザ情報を出力する
			User user = UserDao.findById(id);

			request.setAttribute("user", user);
			RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/userUpdate.jsp");
			dispatcher.forward(request, response);

		}

	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		// TODO Auto-generated method stub
		request.setCharacterEncoding("UTF-8");

		String password = request.getParameter("password");
		String passwordC = request.getParameter("password-confirm");
		String name = request.getParameter("user-name");
		String birthDate = request.getParameter("birth-date");
		String id = request.getParameter("user-id");

		String encodestr = PasswordEncorder.encordPassword(password);

		UserDao userDao = new UserDao();
		userDao.update(encodestr, name, birthDate, id);
		

		if (name.equals("")) {
			request.setAttribute("errMsg", "入力した内容は正しくありません。");
			User user = UserDao.findById(id);
			request.setAttribute("user", user);
			RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/userUpdate.jsp");
			dispatcher.forward(request, response);
		}
		else if (password.equals("")) {
			request.setAttribute("errMsg", "入力した内容は正しくありません。");
			User user = UserDao.findById(id);
			request.setAttribute("user", user);
			RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/userUpdate.jsp");
			dispatcher.forward(request, response);
		}
		else if (birthDate.equals("")) {
			request.setAttribute("errMsg", "入力した内容は正しくありません。");
			User user = UserDao.findById(id);
			request.setAttribute("user", user);
			RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/userUpdate.jsp");
			dispatcher.forward(request, response);
		}
		else if (passwordC.equals("")) {
			request.setAttribute("errMsg", "入力した内容は正しくありません。");
			User user = UserDao.findById(id);
			request.setAttribute("user", user);
			RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/userUpdate.jsp");
			dispatcher.forward(request, response);

		}else

		// ユーザー一覧サーブレットにリダイレクト
		response.sendRedirect("UserListServlet");
	}

}
