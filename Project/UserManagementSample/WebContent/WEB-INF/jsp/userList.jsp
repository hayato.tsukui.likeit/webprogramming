<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!-- DataFormat用 -->
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>

<!DOCTYPE html>
<html lang="ja">
<head>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<title>ユーザ一覧画面</title>
<!-- BootstrapのCSS読み込み -->
<link rel="stylesheet"
	href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css"
	integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T"
	crossorigin="anonymous">
<!-- オリジナルCSS読み込み -->
<link href="css/original/common.css" rel="stylesheet">
</head>
<body>
	<!-- header -->
	<header>
		<nav class="navbar navbar-expand-lg navbar-dark bg-dark mb-3">
			<div class="container">
				<a class="navbar-brand" href="LoginServlet">ユーザ管理システム</a>

				<div class="collapse navbar-collapse">
					<ul class="navbar-nav ml-auto">
						<li class="nav-item"><span class="navbar-text text-dark">
								</span><span class="navbar-text">${userInfo.name}さん</span></li>
						<li class="nav-item"><a class="nav-link text-danger"
							href="LogoutServlet">ログアウト</a></li>
					</ul>
				</div>
			</div>
		</nav>
	</header>
	<!-- /header -->

	<!-- body -->
	<div class="container">
		<div class="row mb-3">
			<div class="col text-right">
				<a href="UserAddServlet">新規登録</a>
			</div>
		</div>

		<div class="card">
			<div class="card-header">
				<h5>検索条件</h5>
			</div>
			<div class="card-body">

				<form method="POST" action="UserListServlet">
					<div class="form-group row">
						<label for="code" class="control-label col-3">ログインID</label>
						<div class="col-9">
							<input type="text" name="login-id" id="login-id"
								class="form-control" value="${loginId}">
						</div>
					</div>
					<div class="form-group row">
						<label for="name" class="control-label col-3">ユーザ名</label>
						<div class="col-9">
							<input type="text" name="user-name" id="user-name"
								class="form-control" value="${userName}">
						</div>
					</div>
					<div class="form-group row">
						<label for="continent" class="control-label col-3">生年月日</label>
						<div class="col-9">
							<div class="row">
								<div class="col-5">
									<input type="date" name="start-date" id="start-date"
										class="form-control" size="30" value="${startDate}">
								</div>
								<div class="col-2 text-center">~</div>
								<div class="col-5">
									<input type="date" name="end-date" id="end-date"
										class="form-control" value="${endDate}">
								</div>
							</div>
						</div>
					</div>
					<div class="form-group row">
						<div class="col text-right">
							<button type="submit" value="検索"
								class="btn btn-primary form-submit">検索</button>
						</div>
					</div>
				</form>
			</div>

			<table class="table table-striped">
				<thead>
					<tr>
						<th>ログインID</th>
						<th>ユーザ名</th>
						<th>生年月日</th>
						<th></th>
					</tr>
				</thead>
				<tbody>
					<c:forEach var="user" items="${userList}">
						<tr>
							<td>${user.loginId}</td>
							<td>${user.name}</td>
							<td><fmt:formatDate value="${user.birthDate}"
									pattern="yyyy年MM月dd日" /><br></td>
							<!-- TODO 未実装；ログインボタンの表示制御を行う -->
							<td class="float-right"><a class="btn btn-primary"
								href="UserDetailServlet?id=${user.id}">詳細</a> <a
								class="btn btn-success" href="UserUpdateServlet?id=${user.id}">更新</a>
								<a class="btn btn-danger" href="UserDeleteServlet?id=${user.id}">削除</a></td>
						</tr>
					</c:forEach>
				</tbody>
			</table>
		</div>
	</div>
</body>
</html>
