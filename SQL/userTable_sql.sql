create database usermanagement; 

use usermanagement; 

create table user ( 
    id serial primary key
    , login_id varchar (255) unique not null
    , name varchar (255) not null
    , birth_date DATE not null
    , password varchar (255) not null
    , is_admin boolean not null
    , create_date DATETIME not null
    , update_date DATETIME not null
); 

use usermanagement; 

insert 
into user 
values ( 
    null
    , 'admin'
    , '管理者'
    , '1999-07-17'
    , 'password'
    , true
    , now()
    , now()
); 

ALTER TABLE user CONVERT TO CHARACTER SET utf8mb4;

SELECT * FROM user WHERE login_id = 'admin' and password = 'password';

SELECT * FROM user WHERE login_id = ? and password = ?;
use usermanagement;

ALTER TABLE user ALTER COLUMN is_admin SET DEFAULT false;

select * from user where id = 2 ;
select * from user where login_id = 'あ';

update user set password = 'c', name = 'c' , birth_date = '1999-07-17'  where login_id = 'p';

delete from user where login_id = 'あ';



全部記入
select * from user 
where is_admin = false 
and login_id =  '山下岳'
and name like '%下%' 
and birth_date >= '1922-03-01' 
and birth_date <= '2022-03-01'
;
